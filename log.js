var { createLogger, transports ,format} = require('winston');
require('winston-daily-rotate-file');
const fs = require('fs');
const path = require('path');
const logDir = 'log';
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}
const dailyRotateFileTransport = new transports.DailyRotateFile({
    filename: `${logDir}/CommissionBackend/%DATE%-results.log`,
    datePattern: 'YYYY-MM-DD'
});
var logger = createLogger({
    format: format.combine(
        format.json(),
        format.timestamp( {format: 'YYYY-MM-DD HH:mm:ss'}),
        format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
    ),
  transports: [
    new transports.Console({
        level: 'info',
        format: format.combine(
          format.colorize(),
          format.printf(
            info => `${info.timestamp} ${info.level}: ${info.message}`
          )
        )
      }),
      dailyRotateFileTransport
  ],
  exitOnError: false
});

module.exports = logger;