'use strict';
const express = require('express');
require('dotenv').config()
const path = require('path');
const app = express();
const axios = require('axios');

const port = process.env.PORT || 8081;
const bodyParser = require('body-parser');
const cors = require('cors');
//const multipart = require('connect-multiparty');
//const multer = require('multer');
require('./src/database/mongo');
const SFCredential = require('./config/config.salesforce');
const { apiUrl, portalUrl } = require('./config/backend.url');

const decode = require('salesforce-signed-request');
let consumerSecret = SFCredential.clientSecret;

const { decryptApiKey } = require('./src/utils/tools');
const consoleHelpers = require('./src/utils/consoleHelpers');

app.use(cors());
app.options('*', cors());
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

//app.use(multipart());
//app.use(multer());
app.use(bodyParser.json({ extended: true, limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

app.use('/v0.1/', decryptApiKey);
require('./src/versions/v1')(app);

// added this to debug maxListener Event error
process.on('warning', e => consoleHelpers.warn(e.stack));
app.listen(port, () => {
  console.log(`Project api on port: ${port}`); // eslint-disable-line
});

app.post('/SSO', (req, res) => {
  //console.log(req);
  var signedRequest = decode(req.body.signed_request, consumerSecret),
    context = signedRequest.context,
    oauthToken = signedRequest.client.oauthToken,
    instanceUrl = signedRequest.client.instanceUrl
  axios.post(`${apiUrl}/v0.1/updateOAuthToken`, {
    SalesForceId: signedRequest.userId,
    oauthToken,
    refreshToken: signedRequest.client.refreshToken,
    instanceUrl,
    user: signedRequest.context.user
  }).then((response) => {
    console.log(`statusCode: ${response}`)
    // res.redirect(`${portalUrl}/layout/salesforce/${response.data.token}`);
     res.redirect(`${portalUrl}/layout/salesforce/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZjQ0ZGU4NzlmMDZhNTYyYzRhOTZlOWIiLCJ1c2VybmFtZSI6ImNvbW1pc3Npb25oZWxwZXJAbW9yZXllYWhzLmluIiwiaWF0IjoxNjAwMjY3Mjk0LCJleHAiOjE2MDg5MDcyOTR9.7g6vBbYuAV9fV5SxQIa3aiyka1QlFfRHffGBSq00gH4/home`);

  })
    .catch((error) => {
      console.error(error);
      res.redirect(`${portalUrl}/layout/notfound`);
    })

});

module.exports = app;
