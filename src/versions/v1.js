const adminRoutes = require('../routes/v0.1/admin.routes');
const authRoutes = require('../routes/v0.1/auth.routes');
const userRoutes = require('../routes/v0.1/user.routes');
const teamRoutes = require('../routes/v0.1/team.routes');
const PlanRoutes = require('../routes/v0.1/Plan.routes');
const quotaRoutes = require('../routes/v0.1/quota.routes');
const UserToPlanRoutes = require('../routes/v0.1/UserToPlan.routes');
const PlanToTeamRoutes = require('../routes/v0.1/PlanToTeam.routes');
const productMasterRoutes=require('../routes/v0.1/productMaster.routes')
module.exports = function (app) {
  app.use('/v0.1/', [
    adminRoutes,
    authRoutes,
    userRoutes,
    teamRoutes,
    PlanRoutes,
    quotaRoutes,
    UserToPlanRoutes,
    productMasterRoutes,
    PlanToTeamRoutes
    // ImageRoutes
  ]);
};
