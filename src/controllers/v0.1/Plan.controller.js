let PatrolMan = require('patrolman');
const tools = require('../../utils/tools');
const TaError = require('../../utils/taerror'); // eslint-disable-line
const validator = require('../../utils/validator');
const { OK, Created, Bad_Request, Unauthorized, Server_Error } = require('../../utils/error.def');
const crypto = require('crypto');
PatrolMan = new PatrolMan(require('../../policies/config'));
const mongoose = require('mongoose');
const PlanSystem = mongoose.model('Plan');
const ObjectId = require('mongodb').ObjectID;
const config = ("dotenv");
var logger = require("../../../log");
const { db, distinct } = require('../../models/Plan');

const PlanController = {

    addPlan: async (req, res) => {
        try {
            let {
                // AddPlan,// RenamePlan,// MonthStart,// MonthEnd,// Year,// Region,// Facality,
                // CommissionPercentage,// StoreValues2,// planValue
                planName,commissionAmount
            } = req.body;
            let paramErrors = new TaError(400);

            //if (!AddPlan || !validator.isValidString(AddPlan))
            if (!planName || !validator.isValidString(planName)) {
                paramErrors.addRequestError('invalid Plan Name please Check');
            }
            // if (!Year || !validator.isValidNumber(Year)) {
            //     paramErrors.addRequestError('invalid Plan number please Check');
            // }
            // if (!Region || !validator.isValidString(Region)) {
            //     paramErrors.addRequestError('invalid Plan number please Check');
            // }
            if (paramErrors.isErrors()) {
                return res.status(400).json({
                    success: false,
                    error: paramErrors,
                    message: "Invalid Parameters"
                });
            }
            const PlanObj = {
                // AddPlan,// RenamePlan,// MonthStart,// MonthEnd,// Year,// Region,// Facality,
                // CommissionPercentage,// StoreValues2,// planValue
                planName,commissionAmount
            }
            const newPlanModule = new PlanSystem(PlanObj);
            let savePlan = await newPlanModule.save();
            console.log('result:', savePlan);
            if (savePlan) {
                return res.status(200).json({
                    success: true,
                    message: "Plan Added Successfully",
                    savePlan
                });
            } else {
                return res.status(400).json({
                    success: false,
                    messages: "unable to Add Plan ",
                    savePlan
                });
            }
        } catch (error) {
            console.log('Error in Adding Plan:', error);
            logger.error(`${error} ${req.originalUrl}`);
            res.status(Server_Error).json({
                success: false,
                error: error
            });
        }
    },
    ModifyPlan: async (req, res) => {
        try {
            //const {PlanId}= req.query
            const PlanId=req.query._id;
            let {
                // AddPlan,// Year,// Region,// CommissionPercentage,// StoreValues2
                planName,commissionAmount,users
            } = req.body
            //let data={};
            // if (planName) {
            //     data.planName = planName;
            // }
            // if (commissionAmount) {
            //     data.commissionAmount = commissionAmount;
            // }
            // if (users) {
            //     data.users = users;
            // }
            let data = {};
            if(planName){
                data.planName=planName
            }
            if(commissionAmount){
                data.commissionAmount=commissionAmount
            }
            if(users){
                data.users=users
            }
            
            const updatePlan = await PlanSystem.findOneAndUpdate({ _id: PlanId },
                { $set: data },
                {useFindAndModify:false});
            if (!updatePlan) {
                return res.status(400).json({
                    success: false,
                    message: "Unable To Modify Plan"
                });
            }
            return res.status(200).json({
                success: true,
                message: "Plan Modified Successfully",
                updatePlan
            })
        } catch (error) {
            console.log('Errors in Modify Plan', error)
            logger.error(`${error} ${req.originalUrl}`);
            return res.status(Server_Error).json({
                success: false, error: error.message
            });
        }
    },
    getAllPlans: async (req, res) => {
        try {
            const allplans = await PlanSystem.find({});

            if(!allplans){
                return res.status(400).json({
                    success:false,
                    message: "Plan not found"
                })
            }
            return res.status(200).json({
                success: true,
                message: "All Plans Fetched!",
                allplans
            });
        } catch (error) {
            console.log(' Error:', error);
            logger.error(`${error} ${req.originalUrl}`);
            res.status(Server_Error).json({
                success: false,
                message: 'server error',error
            });
        }
    },
    removePlan: async (req, res) => {
        try {
            const {_id}= req.query;
            const removePlan = await PlanSystem.deleteOne({_id: _id})
            if(!removePlan){
                return res.status(400).json({
                    success: false,
                    message: 'Unable To Remove Plan',
                    removePlan
                });
            }
                return res.status(200).json({
                    success: true,
                    message: "Plan Removed Successfully",
                    removePlan
                });
            
        } catch (error) {
            console.log('Error',error);
            return res.status(500).json({
            success: false,
            message: 'server error',error
        });
        }
    },
    obtainPlanById: async (req, res) => {
        try {
          //const {PlanId}= req.query;
          const PlanId=req.query._id;
          console.log(req.query);

          let fetchPlan = await PlanSystem.findOne({_id: PlanId});
          if(!fetchPlan){
            return res.status(400).json({
                success: false,
                message:"Plan not found"
            })
        }
          return res.status(200).json({
            success: true,
            message: "Plan Obtained Successfully",
            fetchPlan
          });
    
        } catch (error) {
          console.log('Cannot Obtain Plan:', error);
          logger.error(`${error} ${req.originalUrl}`);
          res.status(Server_Error).json({
            success: false,
            error: error
          });
        }
  }, 
}
module.exports = PatrolMan.patrol('Plan', PlanController)