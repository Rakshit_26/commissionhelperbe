let PatrolMan = require('patrolman');
const tools = require('../../utils/tools');
const TaError = require('../../utils/taerror'); // eslint-disable-line
const validator = require('../../utils/validator');
const { OK, Created, Bad_Request, Unauthorized, Server_Error } = require('../../utils/error.def');
const crypto = require('crypto');
PatrolMan = new PatrolMan(require('../../policies/config'));
const mongoose = require('mongoose');
const Users = mongoose.model('Users');
const ObjectId = require('mongodb').ObjectID;
const config = ("dotenv");
var logger = require("../../../log");

const UserController = {

    addUser: async (req, res) => {
        try {
            const { username } = req.body;
            let { userType, Name, contactNumber, Company, Role, password } = req.body;



            const UserDetails = req.currentUser;

            if (!UserDetails) {
                return res.status(400).json({
                    success: false, error: 'Not a valid user'
                });
            }

            if (UserDetails.userType == 1) {


                let paramErrors = new TaError(Bad_Request);

                if (!username || !validator.isValidEmail(username)) {
                    paramErrors.addRequestError('Invalid email.');
                }
                if (!password || !validator.isValidPassword(password)) {
                    paramErrors.addRequestError('Invalid password.');
                }
                // if (!firstname || !validator.isValidString(firstname)) {
                //     paramErrors.addRequestError('Invalid firstname.');
                // }
                // if (!lastname || !validator.isValidString(lastname)) {
                //     paramErrors.addRequestError('Invalid lastname.');
                // }

                if (!Role || !validator.isValidString(Role)) {
                    paramErrors.addRequestError('Inavlid Role');
                }


                if (paramErrors.isErrors()) {
                    return res.status(paramErrors.code).json({ success: false, error: paramErrors, message: "Invalid Parameters" });
                }

                if (!userType) {
                    userType = UserType.user;
                }
                let email = username.toLowerCase();

                const emailRegex = new RegExp(`^${email.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&')}$`, 'i');
                const findUser = await Users.findOne({ 'username': emailRegex });

                if (findUser && findUser._id && findUser.username) {
                    return res.status(OK).json({ success: false, message: 'Email is already registered.', error: new TaError(OK).addParamError('Email is already registered.') });
                }

                // Convert pass to hash & salt
                const { encrypted, salt } = await tools.saltPassword(password);
                const emailHash = crypto.randomBytes(20).toString('hex');

                const usrObj = {
                    Name, username, Company, password: encrypted, emailHash, salt, userType, Role,
                    contactNumber
                };
                const newUser = new Users(usrObj);
                let saveUser = await newUser.save();


                if (!saveUser || !saveUser.username || saveUser.username !== username) {
                    return res.status(OK).json(
                        {
                            success: false,
                            error: new TaError(OK).addParamError('Unable to register user.'),
                            message: 'Unable to register user.'
                        });
                }

                // const token = jwt.sign(
                //     { userId: saveUser.id, username: saveUser.username },
                //     encryptConfig.secret,
                //     { expiresIn: "100d" }
                // );

                delete saveUser.password;
                delete saveUser.salt;
                delete saveUser.emailHash;
                delete saveUser.encrypted;
                delete saveUser._id;

                return res.status(Created).json({
                    success: true,
                    message: "success",
                    user: saveUser,
                    newUser: true
                });
            } else {
                return res.status(400).json({ success: false, error: 'Unauthorized' });
            }

        } catch (error) {
            console.log('signup error:', error);
            return res.status(Server_Error).json({ success: false, error: error });
        }
    },

    deleteUser: async (req, res) => {
        try {
            const { _id
            } = req.query;
            const repelUser = await Users.deleteOne({ _id: _id })
            if (!repelUser) {
                return res.status(400).json({
                    success: false,
                    //message: 'Unable To Delete Plan',
                    message: 'Unable To Delete User',
                    repelUser
                });
            }
            return res.status(200).json({
                success: true,
                message: "User Deleted Successfully",
                repelUser
            });

        } catch (error) {
            console.log('Error', error);
            return res.status(500).json({
                success: false,
                message: 'server error', error
            });
        }
    },

    editUser: async (req, res) => {
        try {
            const userId = req.query._id
            let {
                username,
                Name,
                userType,
                contactNumber,
                Company,
                Role
            } = req.body
            let data = {};
            if (username) {
                data.username = username
            }
            if (Name) {
                data.Name = Name
            }
            if (userType) {
                data.userType = userType
            }
            if (contactNumber) {
                data.contactNumber = contactNumber
            }
            if (Company) {
                data.Company = Company
            }
            if (Role) {
                data.Role = Role
            }

            const updateUser = await Users.findOneAndUpdate({ _id: userId }, { $set: data }, { useFindAndModify: false });
            if (!updateUser) {
                return res.status(400).json({
                    success: false,
                    message: "Unable To Edit User"
                });
            }
            return res.status(200).json({
                success: true,
                message: "User Edited Successfully",
                updateUser
            })
        } catch (error) {
            logger.error(`${error} ${req.originalUrl}`);
            return res.status(Server_Error).json({ success: false, error: error.message });
        }
    },

    getAllUser: async (req, res) => {
        // try {
        //     const {
        //         pageSize, pageIndex, order
        //     } = req.query;
        //     let { property } = req.query;
        //     if (order == 'desc') {
        //         property = '-' + property
        //     }
        //     let UserList = [];
        //     const totalCount = await Users.countDocuments();
        //     UserList = await Users.find()
        //         .sort(property)
        //         .skip(Number(pageSize) * (Number(pageIndex - 1)))
        //         .limit(Number(pageSize));

        //     return res.status(200).json({
        //         success: true,
        //         UserList,
        //         totalCount
        //     })
        // } catch (error) {
        //     logger.error(`${error} ${req.originalUrl}`);
        //     return res.status(404).send({ success: false, "message:": error });
        // }
        try {
            // const UserDetails = req.currentUser;

            // if (!UserDetails) {
            //     return res.status(400).json({
            //         success: false, error: 'Not a valid user'
            //     });
            // }

            //   if (UserDetails.userType == 1) {
            let getall = await Users.find({});
            return res.status(OK).json({
                success: true,
                message: 'All Users Info Fetched',
                getall
            });
            // } else {
            //     return res.status(400).json({ success: false, error: 'Unauthorized' });
            // }


        } catch (error) {
            console.log('error:', error);
            res.status(Server_Error).json({
                success: false,
                error: error
            });
        }
    },

    obtainUserById: async (req, res) => {
        try {
            const { UId
            } = req.query;
            console.log(req.query);

            let catchUser = await Users.findOne({ _id: UId });
            if (!catchUser) {
                return res.status(400).json({
                    success: false,
                    message: "User Not Found"
                })
            }
            return res.status(200).json({
                success: true,
                message: "User Obtained Successfully",
                catchUser
            });

        } catch (error) {
            console.log('Cannot Search Any Single User:', error);
            logger.error(`${error} ${req.originalUrl}`);
            res.status(Server_Error).json({
                success: false,
                error: error
            });
        }
    },
}
module.exports = PatrolMan.patrol('user', UserController);