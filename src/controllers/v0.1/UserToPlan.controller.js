let PatrolMan = require('patrolman');
const tools = require('../../utils/tools');
const TaError = require('../../utils/taerror'); // eslint-disable-line
const validator = require('../../utils/validator');
const { OK, Created, Bad_Request, Unauthorized, Server_Error } = require('../../utils/error.def');
const crypto = require('crypto');
PatrolMan = new PatrolMan(require('../../policies/config'));
const mongoose = require('mongoose');
const AddSystem = mongoose.model('UserToPlan');
const ObjectId = require('mongodb').ObjectID;
const config = ("dotenv");
var logger = require("../../../log");

const UserToPlanController = {
    AddUserToPlan: async (req, res) => {
        try {
            let {
                Spiffer,
                Email,
                Role,
                EffectiveAsOf,
                EffectiveUntil
            } = req.body;
            if(!Spiffer || !validator.isValidString(Spiffer)){
                paramErrors.addRequestError('Inavlid String');
            }
            if(!Email || !validator.isValidString(Email)){
                paramErrors.addRequestError('Inavlid String');
            }
            if(!Role || !validator.isValidString(Role)){
                paramErrors.addRequestError('Inavlid String')
            }
            let paramErrors = new TaError(400);

            if (paramErrors.isErrors()) {
                return res.status(400).json({
                    success: false,
                    error: paramErrors,
                    message: "Invalid Parameters"
                });
            }
            const AUTPObj = {
                Spiffer,
                Email,
                Role,
                EffectiveAsOf,
                EffectiveUntil
            }
            const newAddModule = new AddSystem(AUTPObj);
            let saveAUTP = await newAddModule.save();
            console.log('result:', saveAUTP);
            if (saveAUTP) {
                return res.status(200).json({
                    success: true,
                    message: "User To Plan Added Successfully",
                    saveAUTP
                });
            } else {
                return res.status(400).json({
                    success: false,
                    messages: "unable to Add User Plan ",
                    saveAUTP
                });
            }
        } catch (error) {
            console.log('Error in Adding User To Plan:', error);
            logger.error(`${error} ${req.originalUrl}`);
            res.status(Server_Error).json({
                success: false,
                error: error

            });
        }
    },
    ModifyUserToPlan: async (req, res) => {
        try {
            const UserId = req.query;
            let {
                Spiffer,
                Email,
                Role,
            } = req.query
            let data = {};
            if (Spiffer) {
                data.Spiffer = Spiffer;
            }
            if (Email) {
                data.Email = Email;
            }
            if (Role) {
                data.Role= Role;
            }
            const updatePlanTs = await AddSystem.findOneAndUpdate({ _id: UserId },
                { $set: data },
                { useFindAndModify: false });
            if (updatePlanTs) {
                return res.status(OK).json({
                    success: true,
                    message: "Plan Updated Successfully"
                });
            }
        } catch (error) {
            console.log('Errors in Modify Plan', error);
            logger.error(`${error} ${req.originalUrl}`);
            return res.status(Server_Error).json({
                success: false, error: error.message
            });
        }
    },
    SearchUserToPlan: async (req, res) => {
        try {
            const {UserId
          } = req.body;
            console.log(req.body);
  
            let srchPlan = await AddSystem.findOne({_id: UserId});
            if(!srchPlan){
                return res.status(400).json({
                    success: false,
                    message: 'Unable To Search User To Plan'
                });
            }
            return res.status(200).json({
              success: true,
              message: "User Searched Successfully From Plan",
              srchPlan
            });
      
          } catch (error) {
            console.log('Cannot Search Plan From Team:', error);
            logger.error(`${error} ${req.originalUrl}`);
            res.status(Server_Error).json({
              success: false,
              error: error
            });
          }
    },
    RemoveUserToPlan: async (req, res) => {
        try {
            const {_id
            } = req.query;
            const removePlanTS = await AddSystem.findByIdAndDelete({_id: _id})
            if(removePlanTS){
                return res.status(400).json({
                    success: false,
                    message: 'Unable to Remove User To Plan'
                })
            }
                return res.status(200).json({
                    success: true,
                    message: "Plan Removed Successfully",
                    removePlanTS
                });
            
        } catch (error) {
            console.log('Error',error);
            return res.status(500).json({
            success: false,
            message: 'server error',error
        });
        }
    },
    
}
module.exports = PatrolMan.patrol('UserToPlan',UserToPlanController)