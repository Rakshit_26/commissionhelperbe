
let PatrolMan = require('patrolman');
const tools = require('../../utils/tools');
const TaError = require('../../utils/taerror');
const validator = require('../../utils/validator');
const encryptConfig = require('../../../config/config.encrypt');
const { UserType, UserStatus } = require('../../datatypes/User');
const { OK, Created, Bad_Request, Unauthorized, Server_Error } = require('../../utils/error.def');

PatrolMan = new PatrolMan(require('../../policies/config'));
const mongoose = require('mongoose');

// Models
const Users = mongoose.model('Users');

const AdminController = {

  listAll: async (req, res) => {
    try {
      let users = await Users.find({});

      //   if(users.length === 0){
      //    return res.status(OK).json({success: true, users, message:'no user exists in database'});
      //   }
      //   else{
      //    return res.status(OK).json({success: true, users, message:'success'})
      //   }
      return res.status(OK).json({ success: true, users, message: 'success' })
    }
    catch (error) {
      console.log('error:', error);
      return res.status(Server_Error).send({ success: true, error });
    }
  },


  
}

module.exports = PatrolMan.patrol('admin', AdminController);