let PatrolMan = require('patrolman');
const tools = require('../../utils/tools');
const TaError = require('../../utils/taerror'); // eslint-disable-line
const validator = require('../../utils/validator');
const { OK, Created, Bad_Request, Unauthorized, Server_Error } = require('../../utils/error.def');
const config = ("dotenv");
PatrolMan = new PatrolMan(require('../../policies/config'));
var logger = require("../../../log");
const mongoose = require('mongoose');


// Models
const product = mongoose.model('productMaster');
const productGroup = mongoose.model('productGroup');


const productMasterController = {

    addProduct: async (req, res) => {
        try {
            let { productName, price, description, unitQuantity, productCode,
                priceBook, category, createdBy } = req.body
            const UserDetails = req.currentUser;

            if (!UserDetails) {
                return res.status(400).json({
                    success: false, error: 'Not a valid user'
                });
            }
            createdBy = UserDetails._id;

            let paramErrors = new TaError(400);

            if (!productName || !validator.isValidString(productName)) {
                paramErrors.addRequestError('Invalid String please Check');
            }

            if (paramErrors.isErrors()) {
                return res.status(400).json({
                    success: false,
                    error: paramErrors,
                    message: "Invalid Parameters"
                });
            }

            const productObj = {
                productName, price, description, unitQuantity, productCode,
                priceBook, category, createdBy
            }

            const newproductModule = new product(productObj)
            const saveProduct = await newproductModule.save()
            console.log('Result', saveProduct);

            if (saveProduct) {
                return res.status(200).json({
                    success: true,
                    message: 'Product Added Successfully',
                    saveProduct
                })

            } else {
                return res.status(200).json({
                    success: false,
                    message: 'Unable To Add Product',
                })
            }
        } catch (error) {
            console.log('Error in Adding Products:', error);
            logger.error(`${error} ${req.originalUrl}`);
            res.status(Server_Error).json({
                success: false,
                error: error
            });
        }
    },

    editProduct: async (req, res) => {
        try {
            const ProductId = req.query._id;
            let {
                productName, price, description, unitQuantity, productCode,
                priceBook, category
            } = req.body

            let data = {};
            if (productName) {
                data.productName = productName
            }
            if (price) {
                data.price = price
            }
            if (description) {
                data.description = description
            }
            if (unitQuantity) {
                data.unitQuantity = unitQuantity
            }
            if (productCode) {
                data.productCode = productCode
            }
            if (priceBook) {
                data.priceBook = priceBook
            }
            if (category) {
                data.category = category
            }

            const updateProduct = await product.findOneAndUpdate({ _id: ProductId },
                { $set: data },
                { useFindAndModify: false });

            if (!updateProduct) {
                return res.status(400).json({
                    success: false,
                    message: "Unable To Update Products"
                });
            }
            return res.status(200).json({
                success: true,
                message: "Products Updated Successfully",
                updateProduct
            })

        } catch (error) {
            console.log('Errors in Update Product', error)
            logger.error(`${error} ${req.originalUrl}`);
            return res.status(Server_Error).json({
                success: false, error: error.message
            });
        }
    },

    deleteProduct: async (req, res) => {
        try {
            const { _id } = req.query;

            const deleteProduct = await product.deleteOne({ _id: _id });

            if (!deleteProduct) {
                return res.status(400).json({
                    success: false,
                    message: "Error Occurred in deleteProduct",
                    deleteProduct
                })
            }

            return res.status(OK).json({
                success: true,
                message: 'Product Deleted Successfully',
                deleteProduct
            })

        } catch (error) {
            console.log('Error in Delete Quota', error)
            logger.error(`${error} ${req.originalUrl}`);
            return res.status(Server_Error).json({
                success: false,
                error: error.message
            });
        }
    },

    getAllProduct: async (req, res) => {
        try {
            const allProducts = await product.find({})

            if (!allProducts) {
                return res.status(400).json({
                    success: false,
                    message: "Products not found"
                })
            }

            return res.status(200).json({
                success: true,
                message: "All Products Get Successfully",
                allProducts
            })

        } catch (error) {
            console.log('Error in Get All Products:', error);
            logger.error(`${error} ${req.originalUrl}`);
            res.status(Server_Error).json({
                success: false,
                error: error
            });
        }
    },

    getProductById: async (req, res) => {
        try {
            const ProductId = req.query._id;
            console.log(req.query);

            let getProduct = await product.findOne({ _id: ProductId });

            if (!getProduct) {
                return res.status(400).json({
                    success: false,
                    message: "Products not found"
                })
            }

            return res.status(200).json({
                success: true,
                message: "Get Product Successfully",
                getProduct
            });

        } catch (error) {
            console.log('Cannot Get Quota By Id:', error);
            logger.error(`${error} ${req.originalUrl}`);
            res.status(Server_Error).json({
                success: false,
                error: error
            });
        }
    },

    addProductGroup: async (req, res) => {
        try {
            let { GroupName, productIds } = req.body
            const UserDetails = req.currentUser;

            if (!UserDetails) {
                return res.status(400).json({
                    success: false, error: 'Not a valid user'
                });
            }
            var createdBy = UserDetails._id;
            var updatedBy = UserDetails._id;



            const productObj = {
                GroupName,
                productIds,
                createdBy,
                updatedBy,
                isActive: true,
                isDeleted: false,
                createdAt : Date.now(),
                updatedAt : Date.now(),

            }

            const newGroupModule = new productGroup(productObj)
            const saveProductGroup = await newGroupModule.save()
            console.log('Result', saveProductGroup);

            if (saveProductGroup) {
                return res.status(200).json({
                    success: true,
                    message: 'Product Added Successfully',
                    saveProductGroup
                })

            } else {
                return res.status(200).json({
                    success: false,
                    message: 'Unable To Add Product',
                })
            }
        } catch (error) {
            console.log('Error in Adding Products:', error);
            logger.error(`${error} ${req.originalUrl}`);
            res.status(Server_Error).json({
                success: false,
                error: error
            });
        }
    },

    editProductGroup: async (req, res) => {
        try {
            let { GroupName, productIds, _id } = req.body
            const UserDetails = req.currentUser;

            if (!UserDetails) {
                return res.status(400).json({
                    success: false, error: 'Not a valid user'
                });
            }
            var createdBy = UserDetails._id;
            var updatedBy = UserDetails._id;



            const productObj = {
                GroupName,
                productIds,
                createdBy,
                updatedBy,
                isActive: true,
                isDeleted: false,
                updatedAt : Date.now(),


            }
            const updateProduct = await productGroup.findOneAndUpdate({ _id: _id },
                { $set: productObj },
                { useFindAndModify: false });

            if (!updateProduct) {
                return res.status(400).json({
                    success: false,
                    message: "Unable To Update Products"
                });
            }
            return res.status(200).json({
                success: true,
                message: "Products Updated Successfully",
                updateProduct
            })

        } catch (error) {
            console.log('Errors in Update Product', error)
            logger.error(`${error} ${req.originalUrl}`);
            return res.status(Server_Error).json({
                success: false, error: error.message
            });
        }
    },

    getAllProductGroup: async (req, res) => {
        try {

            const allProducts = await productGroup.find({ isDeleted: false })

            if (!allProducts) {
                return res.status(400).json({
                    success: false,
                    message: "Products not found"
                })
            }

            return res.status(200).json({
                success: true,
                message: "All Product groups Get Successfully",
                allProducts
            })

        } catch (error) {
            console.log('Error in Get All Products:', error);
            logger.error(`${error} ${req.originalUrl}`);
            res.status(Server_Error).json({
                success: false,
                error: error
            });
        }
    },


    getProductGroupInfo: async (req, res) => {
        try {
            const ProductGroupId = req.query._id;

            const ProductGroupInfo = await productGroup.find({ _id: ProductGroupId })

            if (!allProducts) {
                return res.status(400).json({
                    success: false,
                    message: "Products not found"
                })
            }

            var groupInfo = ProductGroupInfo.productIds;

            const allProducts = await product.find({ _id: { $in: groupInfo } });


            return res.status(200).json({
                success: true,
                message: "All Product groups Get Successfully",
                ProductGroupInfo,
                allProducts
            })

        } catch (error) {
            console.log('Error in Get All Products:', error);
            logger.error(`${error} ${req.originalUrl}`);
            res.status(Server_Error).json({
                success: false,
                error: error
            });
        }
    },





}
module.exports = PatrolMan.patrol('productMaster', productMasterController);