const crypto = require('crypto');
const jwt = require('jsonwebtoken');
let PatrolMan = require('patrolman');
const tools = require('../../utils/tools');
const TaError = require('../../utils/taerror');
const validator = require('../../utils/validator');
const nodemailer = require("nodemailer");
var logger = require("../../../log");
const encryptConfig = require('../../../config/config.encrypt');
const {
    UserType,
    UserStatus
} = require('../../datatypes/User');
const {
    OK,
    Created,
    Bad_Request,
    Unauthorized,
    Server_Error
} = require('../../utils/error.def');
const ObjectId = require('mongodb').ObjectID;
PatrolMan = new PatrolMan(require('../../policies/config'));
const mongoose = require('mongoose');



// Models
// const Users = mongoose.model('Users');
const Users = require('../../models/users')
const AuthController = {

    signup: async (req, res) => {
        try {
            const {
                username,
                Role
            } = req.body;
            let {
                Name,
                password,
                userType
            } = req.body;

            console.log(req.body);
            // Check validations
            let paramErrors = new TaError(Bad_Request);

            if (!username || !validator.isValidEmail(username)) {
                paramErrors.addRequestError('Invalid username.');
            }

            if (!password || !validator.isValidPassword(password)) {
                paramErrors.addRequestError('Invalid password.');
            }


            if (paramErrors.isErrors()) {
                return res.status(paramErrors.code).json({
                    success: false,
                    error: paramErrors,
                    message: "Invalid Parameters"
                });
            }

            // if (!userType) {
            //     userType = UserType.user;
            // }
            var email = username.toLowerCase();


            const emailRegex = new RegExp(`^${email.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&')}$`, 'i');
            const findUser = await Users.findOne({
                'username': emailRegex
            });

            if (findUser && findUser._id && findUser.username) {
                return res.status(OK).json({
                    success: false,
                    message: 'Email is already registered.',
                    error: new TaError(OK).addParamError('Email is already registered.')
                });
            }

            // Convert pass to hash & salt
            const {
                encrypted,
                salt
            } = await tools.saltPassword(password);
            const emailHash = crypto.randomBytes(20).toString('hex');

            const usrObj = {
                username,
                password: encrypted,
                Name, Role,
                emailHash,
                salt,
                userType,
            };
            const newUser = new Users(usrObj);
            let saveUser = await newUser.save();
            console.log(saveUser)

            if (!saveUser || !saveUser.username || saveUser.username !== username) {
                return res.status(OK).json({
                    success: false,
                    error: new TaError(OK).addParamError('Unable to register user.'),
                    message: 'Unable to register user.'
                });
            }

            if (saveUser) {
                const token = jwt.sign({
                    userId: saveUser._id,
                    username: saveUser.username
                },
                    encryptConfig.secret, {
                    expiresIn: "100d"
                });

                delete saveUser.password;
                delete saveUser.salt;
                delete saveUser.emailHash;
                delete saveUser.encrypted;
                delete saveUser._id;

                return res.status(Created).json({
                    success: true,
                    message: "success",
                    token,
                    user: saveUser,
                    newUser: true
                });
            }
        } catch (error) {
            console.log('signup error:', error);
            logger.error(`${error} ${req.originalUrl}`);
            res.status(Server_Error).json({
                success: false,
                error: error
            });
        }
    },

    login: async (req, res) => {
        try {
            let {
                username,
                password
            } = req.body;

            let paramErrors = new TaError(Bad_Request);

            if (!username || !validator.isValidEmail(username)) {
                paramErrors.addRequestError('Invalid Email.');
            }
            if (!password || !validator.isValidPassword(password)) {
                paramErrors.addRequestError('Invalid Password.');
            }
            if (paramErrors.isErrors()) {
                return res.status(paramErrors.code).json({
                    success: false,
                    error: paramErrors,
                    message: "Invalid Parameters"
                });
            }

            username = username.toLowerCase();

            // Look in database for user
            const emailRegex = new RegExp(`^${username.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&')}$`, 'i');
            let retUser = await Users.findOne({
                'username': emailRegex
            });

            // Check if valid user returned, return error if needed
            if (!retUser) {
                return res.status(Bad_Request).json({
                    success: false,
                    message: "Invalid username/password.",
                    error: new TaError(Bad_Request).addMongoError('Invalid username/password.')
                });
            }

            // HASH and SALT password, compare to database password
            // const decryptedPass = await tools.decryptPassword(password, retUser.salt);

            // if (decryptedPass !== retUser.password) {
            //     const error = new TaError(Bad_Request).addParamError('Invalid email or password.');
            //     return res.status(Bad_Request).json({
            //         success: false,
            //         message: error
            //     });
            // }

            const token = jwt.sign({
                userId: retUser._id,
                username: retUser.username,
                // userType: retUser.userType
            },
                encryptConfig.secret, {
                expiresIn: "100d"
            }
            );

            delete retUser.password;
            delete retUser.salt;
            delete retUser.emailHash;
            delete retUser.encrypted;
            delete retUser._id;

            return res.status(OK).json({
                success: true,
                token,
                user: retUser,
            });
        } catch (error) {
            console.log('login error:', error);
            logger.error(`${error} ${req.originalUrl}`);
            return res.status(Server_Error).json({
                success: false,
                error: error
            });
        }
    },

    forgotPassword: async (req, res) => {
        try {
            let {
                username
            } = req.body;

            let paramErrors = new TaError(Bad_Request);

            if (!username || !validator.isValidEmail(username)) {
                paramErrors.addRequestError('Invalid username.');
            }

            if (paramErrors.isErrors()) {
                return res.status(paramErrors.code).json({
                    success: false,
                    error: paramErrors,
                    message: "Invalid Parameters"
                });
            }
            const validateUserName = await Users.findOne({
                username: username
            });
            if (!validateUserName) {
                return res.status(Bad_Request).json({
                    success: false,
                    error: 'invalid username'
                });
            }
            //const string1 = random('alphanumeric')
            const string1 = Math.random().toString(32).slice(2)
            console.log(string1);
            const {
                encrypted,
                salt
            } = await tools.saltPassword(string1);
            const emailHash = crypto.randomBytes(20).toString('hex');
            const data = {
                password: encrypted,
                emailHash,
                salt
            };
            const updatePassword = await Users.findOneAndUpdate({
                username: username
            }, {
                $set: data
            }, {
                useFindAndModify: false
            });
            if (updatePassword) {
                var transporter = nodemailer.createTransport({
                    //
                    host: '',
                    port: '',
                    secure: '', //true for 465, false for other ports
                    //
                    //service: 'gmail',
                    auth: {
                        user: '',
                        pass: ''
                    }
                });
                const mailOptions = {
                    from: '', // sender address
                    to: username, // list of receivers
                    subject: 'Subject of your email', // Subject line
                    html: '<p>Hello there,</p>Your password has been changed. You can login using the temporary password below.<h3>' +
                        string1 + '</h3><p> Commission User change your password', // plain text body
                    // text: "code" + string1
                };
                transporter.sendMail(mailOptions, (err, info) => {
                    if (err)
                        console.log('error', err)
                    else
                        console.log('info', info);
                });
                return res.status(OK).json({
                    success: true,
                    mesagge: "User password change check your email for temporary password",
                    "code": string1,
                });
            }

        } catch (error) {
            logger.error(`${error} ${req.originalUrl}`);
            return res.status(Server_Error).json({
                success: false,
                error: error
            });
        }

    },

    changePassword: async (req, res) => {
        try {
            let {
                token,
                password
            } = req.body;
            const tokenInfo = jwt.decode(token, {
                json: true
            })
            if (!tokenInfo) {
                return res.status(400).json({
                    success: false,
                    message: "invalid token"
                })
            }
            const UserInfo = await Users.findOne({
                _id: tokenInfo.userId
            })
            if (!UserInfo) {
                return res.status(400).json({
                    success: false,
                    message: "User Not Found"
                })
            }
            const {
                encrypted,
                salt
            } = await tools.saltPassword(password);
            const passdata = {
                salt,
                IsMailSent: false,
                Status: 'Active'
            }
            const MailModifyPassword = await Users.findOneAndUpdate({ _id: tokenInfo.userId },
                { $set: passdata },
                { useFindAndModify: false })

            if (MailModifyPassword) {
                return res.status(200).json({
                    success: true,
                    mesagge: "user password is updated and flag changed"
                })
            }
        }
        catch (error) {
            logger.error(`${error} ${req.originalUrl}`);
            return res.status(500).json({
                success: false,
                message: error.message
            })
        }
    },
    updatePassword: async (req, res) => {
        try {
            const oldPassword = req.body.oldPassword;
            const newPassword = req.body.newPassword;


            let paramErrors = new TaError(Bad_Request);
            if (!newPassword || !validator.isValidPassword(newPassword)) {
                paramErrors.addRequestError('Invalid password.');
            }
            if (paramErrors.isErrors()) {
                return res.status(paramErrors.code).json({ success: false, error: paramErrors, message: "Invalid Parameters" });
            }

            const user = req.currentUser;
            if (!user) {
                return res.status(400).json({
                    success: false, error: 'Not a valid user'
                });
            }
            console.log(user);

            const decryptedPass = await tools.decryptPassword(oldPassword, user.salt);
            if (user.password != decryptedPass) {
                return res.status(OK).json({
                    success: false,
                    message: "Old Password is Wrong"
                });
            }

            const { encrypted, salt } = await tools.saltPassword(newPassword);
            const emailHash = crypto.randomBytes(20).toString('hex');
            //const originalPass = newPassword:encrypted;
            const data = { password: encrypted, emailHash, salt };

            const updatePass = await Users.findOneAndUpdate({ _id: req.currentUser._id }, { $set: data }, { useFindAndModify: false });
            if (updatePass) {
                return res.status(OK).json({
                    success: true,
                    message: "Password Changed Succesfully"
                });
            }
        } catch (error) {
            logger.error(`${error} ${req.originalUrl}`);
            return res.status(Server_Error).json({ success: false, error: error.message });
        }


    },


    updateOAuthToken: async (req, res) => {
        try {
            const {
                SalesForceId,
                oauthToken,
                refreshToken,
                instanceUrl,
                user
            } = req.body;
            if (!SalesForceId) {
                return res.status(400).json({
                    success: false,
                    message: 'Invalid User Id'
                });
            }
            var email = 'commissionhelper@moreyeahs.in';
            const userDetails = await Users.findOne({ 'username': email });
            if (!userDetails) {
                return res.status(400).json({
                    success: false,
                    message: 'User Not Found'
                });
            }
            const updateToken = {
                oauthToken,
                refreshToken: refreshToken ? refreshToken : userDetails.refreshToken,
                instanceUrl,
                firstname: user.firstName,
                lastname: user.lastName,
                username: user.userName
            }
            const updateUser = await Users.updateOne({ 'username': email }, { $set: updateToken });
            if (updateUser) {
               // const userDetails = await Users.findOne({ 'username': email });
                const token = jwt.sign({
                    userId: userDetails._id,
                    username: userDetails.username
                },
                    encryptConfig.secret, {
                    expiresIn: "100d"
                }
                );
                return res.status(200).json({
                    success: true,
                    user: userDetails,
                    token
                });
            }
            return res.status(304).json({
                success: true,
                message: 'Auth Token not updated'
            })
        } catch (error) {
            logger.error(`${error} ${req.originalUrl}`);
            return res.status(500).json({
                success: false,
                message: error.message
            })
        }
    },

}


module.exports = PatrolMan.patrol('auth', AuthController);