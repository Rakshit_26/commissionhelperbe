//const express = require("express");
//const bodyParser = require("body-parser");
let PatrolMan = require('patrolman');
const tools = require('../../utils/tools');
const TaError = require('../../utils/taerror'); // eslint-disable-line
const validator = require('../../utils/validator');
const { OK, Created, Bad_Request, Unauthorized, Server_Error } = require('../../utils/error.def');
//const app= express();
const config = ("dotenv");
var logger = require("../../../log");
const mongoose = require('mongoose');
const users = require('../../models/users');
PatrolMan = new PatrolMan(require('../../policies/config'));

const teams = mongoose.model("Team");

const TeamsController = {
    addTeam: async (req, res) => {
        try {
            
            let {
                teamName,numberOfMember, /*createdby,*/ teamMemberName,teamLeadName,
                teamLeadID,teamMemberID,subTeamId,subTeamName,createdBy} = req.body;
                
                const UserDetails = req.currentUser;
                if (!UserDetails) {
                    return res.status(400).json({
                        success: false, error: 'Not a valid user'
                    });
                }

                
            if(teamMemberName!=null){
                numberOfMember=teamMemberName.length}
                
            //if(teamMemberName==null){
            else{
                numberOfMember=0}
            let paramErrors = new TaError(400);
                createdBy=UserDetails._id;
            const teamObj = { teamName, numberOfMember, /*createdby,*/ teamMemberName,
                            teamLeadName,teamLeadID,teamMemberID,subTeamId,subTeamName,createdBy
                            }
            
            if(teamMemberName!=null){
            if(teamMemberName.length==teamMemberID.length && teamLeadName.length<=teamMemberName.length && 
                teamLeadName.length==teamLeadID.length){
                
            const newTeamModule = new teams(teamObj);
            let saveteam = await newTeamModule.save();
            console.log('result:', saveteam);
            if (saveteam) {
                return res.status(200).json({
                    success: true,
                    message: "Team saved successfully",
                    saveteam
                })        }
            else{
                return res.status(400).json({
                    success: false,
                    messages: "unable to save Team ",
                    //savetab
                })
            }} else{
                return res.status(400).json({
                    success: false,
                    messages: "Number of names and ids not equal "  
                })
            }}
            //if(teamMemberName==null && teamMemberID==null && teamLeadID==null && teamLeadName==null){
            else{
                const newTeamModule = new teams(teamObj);
            let saveteam = await newTeamModule.save();
            console.log('result:', saveteam);
            if (saveteam) {
                return res.status(200).json({
                    success: true,
                    message: "Team saved successfully",
                    saveteam
                })
            }
            else{
                return res.status(400).json({
                    success: false,
                    messages: "unable to save Team ",
                    //savetab
                })
            }}
        } catch (error) {
            console.log('signup error:', error);
            logger.error(`${error} ${req.originalUrl}`);
            res.status(Server_Error).json({
                success: false,
                error: error
            });
        }
    },

    deleteTeam: async (req, res) => {
        try {
            const { _id
            } = req.query;
            const delTeam = await teams.deleteOne({ _id: _id });
            if (!delTeam) {
                return res.status(400).json({
                    success: false,
                    message: "Team Deleted Successfully",
                    delTeam
                });
            }
            return res.status(200).json({
                success: true,
                message: "Team Deleted Successfully",
                delTeam
            });

        } catch (error) {
            logger.error(`${error} ${req.originalUrl}`);
            return res.status(Server_Error).json({ success: false, error: error.message });
        }
    },

    getAllTeam: async (req, res) => {
        try {
            
         
            const allTeam = await teams.find({})
            //const data= await teams.find({numberOfMember:{$eq:3}})
            // if(data){
            //     var totLength=data.length;
            //     var totAmount=0;
            //     for(let i=0;i<totLength;i++){
            //         var amount=data[i].numberOfMember;
            //         totAmount+=amount;
            //     }
            // }
            // if(data){
            //     var totAmount=0;
            // var data2= data.map((team)=>{
            //     var amount=team.numberOfMember;
            //     totAmount+=amount;
            // })}
            // if(data){
            //     var totLength=data.length;
            //     var totAmount=0;
            //     for(let i=0,len=data.length;i<len;i++){
            //         var amount=data[i].numberOfMember;
            //         totAmount+=amount;
            //     }
            // }
            

            return res.status(200).json({
                success: true,
                message: "all Teams",
                allTeam,
                //totAmount
            })
        } catch (error) {
            console.log('signup error:', error);
            logger.error(`${error} ${req.originalUrl}`);
            res.status(Server_Error).json({
                success: false,
                error: error

            });
        }
    },
    getAllSubTeam: async (req, res) => {
        try {
            
            let TeamId = req.query._id;            

                
            const allTeam = await teams.find({_id:{$ne:TeamId}})

            return res.status(200).json({
                success: true,
                message: allTeam,
                allTeam
                
                
            })
        } catch (error) {
            console.log('signup error:', error);
            logger.error(`${error} ${req.originalUrl}`);
            res.status(Server_Error).json({
                success: false,
                error: error

            });
        }
    },
    updateTeam: async (req, res) => {
        try {
            const TeamId= req.query._id
            let {
                teamName,numberOfMember,teamMemberName,teamLeadName,
                teamLeadID,teamMemberID,subTeamId,subTeamName
            } = req.body
            let data={};
            if(teamName){
                data.teamName=teamName
            }
            if(numberOfMember){
                data.numberOfMember=numberOfMember
            }
            if(teamMemberName){
                data.teamMemberName=teamMemberName
            }
            if(teamLeadName){
                data.teamLeadName=teamLeadName
            }
            if(teamLeadID){
                data.teamLeadID=teamLeadID
            }
            if(teamMemberID){
                data.teamMemberID=teamMemberID
            }
            if(subTeamId){
                data.subTeamId=subTeamId
            }
            if(subTeamName){
                data.subTeamName=subTeamName
            }
            
            const updateTeam = await teams.findOneAndUpdate({ _id: TeamId },
                { $set: data },
                { useFindAndModify: false });
            if (!updateTeam) {
                return res.status(400).json({
                    success: false,
                    message: "Unable To Modify Team"
                });
            }
            return res.status(200).json({
                success: true,
                message: "Team Modified Successfully",
                updateTeam
            })
        } catch (error) {
            console.log('Errors in Modify Team', error)
            logger.error(`${error} ${req.originalUrl}`);
            return res.status(Server_Error).json({
                success: false, error: error.message
            });
        }
    },
    getTeamById: async (req, res) => {
        try {
            const TeamId= req.query._id;
            console.log(req.query);

            let pickUpTeam = await teams.findOne({ _id:TeamId});
            if (!pickUpTeam) {
                return res.status(400).json({
                    success: false,
                    message: "Team Not Found"
                })
            }
            return res.status(200).json({
                success: true,
                message: pickUpTeam,
                
            });
            } catch (error) {
            console.log('Cannot Search Any Single Team:', error);
            logger.error(`${error} ${req.originalUrl}`);
            res.status(Server_Error).json({
                success: false,
                error: error
            });
        }
    },
    getSubTeamById: async (req, res) => {
        try {
            const subTeamId= req.query._id;
            console.log(req.query);

            let pickUpTeam = await teams.findOne({ _id:subTeamId});
            if (!pickUpTeam) {
                return res.status(400).json({
                    success: false,
                    message: "SubTeam Not Found"
                })
            }
            return res.status(200).json({
                success: true,
                message: "SubTeam Obtained Successfully By Id",
                pickUpTeam
            });
            } catch (error) {
            console.log('Cannot Search Any Single Team:', error);
            logger.error(`${error} ${req.originalUrl}`);
            res.status(Server_Error).json({
                success: false,
                error: error
            });
        }
    },
}
module.exports = PatrolMan.patrol('Teams', TeamsController);