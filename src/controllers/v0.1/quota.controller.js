let PatrolMan = require('patrolman');
const tools = require('../../utils/tools');
const TaError = require('../../utils/taerror'); // eslint-disable-line
const validator = require('../../utils/validator');
const { OK, Created, Bad_Request, Unauthorized, Server_Error } = require('../../utils/error.def');
//const app= express();
const config = ("dotenv");
PatrolMan = new PatrolMan(require('../../policies/config'));
var logger = require("../../../log");
const mongoose = require('mongoose');
const Plan = require('../../models/Plan');
// const { request } = require('http');

// Models
const quota = mongoose.model('quota');

const quotaController = {

    addQuota: async (req, res) => {
        try {
            //let { quotaname, cadence, StoreValues4} = req.body
            let { quotaname, cadence, names, price} = req.body
            
            let paramErrors = new TaError(400);

            if (!quotaname || !validator.isValidString(quotaname)) {
                paramErrors.addRequestError('Invalid String please Check');
            }

            if (!cadence || !validator.isValidString(cadence)) {
                paramErrors.addRequestError('Invalid String please Check');
            }

            if (paramErrors.isErrors()) {
                return res.status(400).json({
                    success: false,
                    error: paramErrors,
                    message: "Invalid Parameters"
                });
            }

            const quotaObj = {
                //quotaname, cadence, StoreValues4
                quotaname, cadence, names, price
            }

            const newquotaModule = new quota(quotaObj)
            const saveQuota = await newquotaModule.save()
            console.log('Result', saveQuota);

            if (saveQuota) {
                return res.status(200).json({
                    success: true,
                    message: 'Quota Added Successfully',
                    saveQuota
                })
            } else {
                return res.status(200).json({
                    success: false,
                    message: 'Unable To Add Quota',
                })
            }
        } catch (error) {
            console.log('Error in Adding Quota:', error);
            logger.error(`${error} ${req.originalUrl}`);
            res.status(Server_Error).json({
                success: false,
                error: error
            });
        }
    },

    editQuota: async (req, res) => {
        try {
            //const QuotaId = req.query;
            const QuotaId = req.query._id;
            let {
                quotaname, cadence, names, price
                //,StoreValues4
            } = req.body
            let data={};
            if(quotaname){
                data.quotaname=quotaname
            }
            if(cadence){
                data.cadence=cadence
            }
            if(names){
                data.names=names
            }
            if(price){
                data.price=price
            }

            const updateQuota = await quota.findOneAndUpdate({ _id: QuotaId },
                { $set: data },
                { useFindAndModify: false });

            if (!updateQuota) {
                return res.status(400).json({
                    success: false,
                    message: "Unable To Update Quota"
                });
            }
            return res.status(200).json({
                success: true,
                message: "Quota Updated Successfully",
                updateQuota
            })

        } catch (error) {
            console.log('Errors in Update Quota', error)
            logger.error(`${error} ${req.originalUrl}`);
            return res.status(Server_Error).json({
                success: false, error: error.message
            });
        }
    },

    deleteQuota: async (req, res) => {
        try {
            const { _id } = req.query;
            const delQuota = await quota.deleteOne({ _id: _id });
            if (!delQuota) {
                return res.status(400).json({
                    success: false,
                    message: "Error Occurred in deleteQuota",
                    delQuota
                })
            }
            return res.status(OK).json({
                success: true,
                message: 'Quota Deleted Successfully',
                delQuota
            })

        } catch (error) {
            console.log('Error in Delete Quota', error)
            logger.error(`${error} ${req.originalUrl}`);
            return res.status(Server_Error).json({
                success: false,
                error: error.message
            });
        }
    },

    getAllQuota: async (req, res) => {
        try {
            const allQuota = await quota.find({})

            if (!allQuota) {
                return res.status(400).json({
                    success: false,
                    message: "Quota not found"
                })
            }

            return res.status(200).json({
                success: true,
                message: "all Quota Fetched Successfully",
                allQuota
            })

        } catch (error) {
            console.log('Error in Get All Quota:', error);
            logger.error(`${error} ${req.originalUrl}`);
            res.status(Server_Error).json({
                success: false,
                error: error
            });
        }
    },

    getQuotaById: async (req, res) => {
        try {
            //const  QuotaId  = req.query;
            const  QuotaId  = req.query._id;
            console.log(req.query);

            let getQuota = await quota.findOne({ _id: QuotaId });
            if (!getQuota) {
                return res.status(400).json({
                    success: false,
                    message: "Quota not found"
                })
            }
            return res.status(200).json({
                success: true,
                message: "Get Quota Successfully",
                getQuota
            });

        } catch (error) {
            console.log('Cannot Get Quota By Id:', error);
            logger.error(`${error} ${req.originalUrl}`);
            res.status(Server_Error).json({
                success: false,
                error: error
            });
        }
    }

}
module.exports = PatrolMan.patrol('quota', quotaController);