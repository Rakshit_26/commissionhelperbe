let PatrolMan = require('patrolman');
const tools = require('../../utils/tools');
const TaError = require('../../utils/taerror'); // eslint-disable-line
const validator = require('../../utils/validator');
const { OK, Created, Bad_Request, Unauthorized, Server_Error } = require('../../utils/error.def');
//const app= express();
const config = ("dotenv");
var logger = require("../../../log");
const mongoose = require('mongoose');
PatrolMan = new PatrolMan(require('../../policies/config'));
const AddSystem = mongoose.model('PlanToTeam')

const PlanToTeamController = {
    AddPlanToTeam: async (req, res) => {
        try{
            const {
                PlanName,
                Spiffer,
                Email,
                Role,
                EffectiveAsOf,
                EffectiveUntil
            } = req.body;
            if(!PlanName || validator.isValidString(PlanName)){
                paramErrors.addRequestError('Invalid String');
            }
            if(!Spiffer || !validator.isValidString(Spiffer)){
                paramErrors.addRequestError('Inavlid String');
            }
            if(!Email || !validator.isValidString(Email)){
                paramErrors.addRequestError('Inavlid String');
            }
            if(!Role || !validator.isValidString(Role)){
                paramErrors.addRequestError('Inavlid String')
            }
            let paramErrors = new TaError(400);
            if(paramErrors.isErrors()){
                return res.status(400).json({
                    success: false,
                    error: paramErrors,
                    message: 'Invalid Parameters'
                });
            }
            const APTobj = {
                PlanName,
                Spiffer,
                Email,
                Role,
                EffectiveAsOf,
                EffectiveUntil 
            }
            const newAddModule = new AddSystem(APTobj);
            let saveAPT = await newAddModule.save();
            console.log('result',saveAPT);
            if(saveAPT){
                return res.status(200).json({
                    success: true,
                    message: 'Plan To Team Added Successfully',
                    saveAPT
                });
            }else{
                return res.status(400).json({
                    success: false,
                    message: 'Unable To Add Plan To Team'
                })
            }
        }catch(error){
            console.log('Error in Adding Plan To Team',error);
            return res.status(Server_Error).json({
                success: false,
                error: error
            });
        }
    },
    ModifyPlanFromTeam: async (req, res) => {
        try {
            const {PlanId} = req.query
            let{
                PlanName,
                Spiffer,
                Email,
                Role
            } = req.query
            let data = {}
            if(PlanName){
                data.PlanName = PlanName;
            }
            if(Spiffer){
                data.Spiffer = Spiffer;
            }
            if(Email){
                data.Email = Email;
            }
            if(Role){
                data.Role = Role;
            }
            const updatePlanTC = await AddSystem.findOneAndUpdate({_id:PlanId},
                {$set: data},{useFindAndModify: false});
                if(updatePlanTC){
                    return res.status(OK).json({
                        success: true,
                        message: 'Plan Added Successfully to Team'
                    });
                }
        } catch (error) {
            console.log('Error In Updating Plans To Team',error);
            logger.error(`${error} ${req.originalUrl}`);
            return res.status(Server_Error).json({
                success: false,
                error: error
            })
        }
    },
    SearchPlanFromTeam: async (req, res) => {
        try {
            const {PlanId
          } = req.query;
            console.log(req.query);
  
            let getPlan = await AddSystem.findOne({_id:PlanId});
            if(!getPlan){
                return res.status(400).json({
                    success: false,
                    message: 'Unable To Search Plan'
                });
            }
            return res.status(200).json({
              success: true,
              message: "Plan Searched Successfully From Team",
              getPlan
            });
      
          } catch (error) {
            console.log('Cannot Search Plan From Team:', error);
            logger.error(`${error} ${req.originalUrl}`);
            res.status(Server_Error).json({
              success: false,
              error: error
            });
          }
    },
    RemovePlanFromTeam: async (req, res) => {
        try {
            const { _id
            } = req.query
            const deletePlanTeam = await AddSystem.findByIdAndDelete({ _id: _id });

            if (!deletePlanTeam) {
                return res.status(400).json({
                    success: false,
                    message: 'Unable To Remove Plan From Team',
                    deletePlanTeam
                });
            }
                return res.status(200).json({
                    success: true,
                    message: "Plan Deleted Successfully From Team",
                    deletePlanTeam
                });
            
        } catch (error) {
            console.log('Error', error);
            return res.status(500).json({
                success: false,
                message: 'server error', error
            });
        }
    },
}
module.exports = PatrolMan.patrol('PlanToTeam', PlanToTeamController)