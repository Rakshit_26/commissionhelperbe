'use strict';
const cron = require("node-cron");
const mongoose = require('mongoose');
require('dotenv').config()
//let database_URL = 'mongodb://ufm1waqkxzfj7ckqikrh:CJgeh9Tlea9vM9VN2N6v@babg7buz13pfkhk-mongodb.services.clever-cloud.com:27017/babg7buz13pfkhk';
let database_URL = process.env.database_Url;
mongoose.Promise = global.Promise;

const option = {
  // useMongoClient: true,
  autoIndex: false, // Don't build indexes
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 500, // Reconnect every 500ms
  poolSize: 10, // Maintain up to 10 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0
};
mongoose.set('useNewUrlParser', true);
mongoose.set('useCreateIndex', true);
//mongoose.set('useUnifiedTopology', true);
mongoose.connect(database_URL, { useNewUrlParser: true, useUnifiedTopology: true }).then(
  () => { console.log('Mongo Server Connected Successfully...!', database_URL ); }, // eslint-disable-line
  (err) => { console.error('Failed to connect to MongoDB:', err.message); /** handle initial connection error */ }
);


// schema registered here
require('../models/users');
require('../models/teams');
require('../models/Plan');
require('../models/PlanToTeam');
require('../models/UserToPlan');
require('../models/productMaster');
// require('../models/Image');
require('../models/quota');
//require('../models/quotaB');
require('../models/productGroup');
