const TaError = require('../utils/taerror'); // eslint-disable-line
const userType = require('../datatypes/User').UserType;

module.exports = (req, res, next) => {
  if (req.currentUser.userType === userType.admin) {
    return next();
  }

  const paramErrors = new TaError(200);
  paramErrors.addPermError('Invalid Admin Permission');
  return res.status(paramErrors.code).json(paramErrors);
};
