const Policies = require('./index');

module.exports = {
  admin: {
    '*': [Policies.isAdmin]
  },
  ai: {
    '*': [ Policies.isAdmin],
  },
  auth: {
    signup: [Policies.all],
    login: [Policies.all],
    validate: [Policies.all],
    editProfile: [Policies.all],
  },
};