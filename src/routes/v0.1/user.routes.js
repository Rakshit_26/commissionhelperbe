const express = require('express');
const router = express.Router();

const UserController = require('../../controllers/v0.1/user.controller');

router.route("/addUser")
.post(UserController.addUser);

router.route("/editUser")
.post(UserController.editUser);

router.route("/getAllUser")
.get(UserController.getAllUser);

router.route("/deleteUser")
.delete(UserController.deleteUser);

router.route("/obtainUserById")
.get(UserController.obtainUserById);

module.exports = router;
