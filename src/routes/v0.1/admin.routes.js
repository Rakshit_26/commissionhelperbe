const express = require('express');
const router = express.Router();

const AdminController = require('../../controllers/v0.1/admin.mongo.controller');

router.route('/admin/listAll').get(AdminController.listAll);

// router.route('/auth/login')
// .post(AuthController.login);

module.exports = router;