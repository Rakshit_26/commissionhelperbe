const express = require('express');
const router = express.Router();

const AcoountsController = require('../../controllers/v0.1/Accounts.controller');

router.route('/CreateAccount')
.post(AccountsController.CreateAccount);

router.route('/SearchAccount')
.get(AccountsController.SearchAccount);

router.route('/ModifyAccount')
.put(AccountsController.ModifyAccount);

router.route('/RemoveAccount')
.delete(AccountsController.RemoveAccount);

module.exports = router;