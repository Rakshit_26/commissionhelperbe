const express = require('express');
const router = express.Router();

const quotaController = require('../../controllers/v0.1/quota.controller');

router.route("/addQuota")
.post(quotaController.addQuota);

router.route("/deleteQuota")
.delete(quotaController.deleteQuota);

// router.route("/addusertoQuota")
// .put(quotaController.addusertoQuota);

router.route("/getAllQuota")
.get(quotaController.getAllQuota);

router.route("/getQuotaById")
.get(quotaController.getQuotaById);

// router.route("/EditQuota")
// .put(quotaController.EditQuota);

// router.route("/getallUser")
// .get(quotaController.getallUser);

router.route("/editQuota")
.put(quotaController.editQuota);

module.exports = router;