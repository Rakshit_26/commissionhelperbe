const express = require('express');
const router = express.Router();

const UserToPlanController = require('../../controllers/v0.1/UserToPlan.controller')

router.route("/AddUserToPlan")
.post(UserToPlanController.AddUserToPlan);

router.route("/RemoveUserToPlan")
.delete(UserToPlanController.RemoveUserToPlan);

router.route("/ModifyUserToPlan")
.put(UserToPlanController.ModifyUserToPlan);

router.route("/SearchUserToPlan")
.get(UserToPlanController.SearchUserToPlan)

module.exports = router;