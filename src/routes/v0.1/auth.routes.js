const express = require('express');
const router = express.Router();

const AuthController = require('../../controllers/v0.1/auth.mongo.controller');

router.route('/auth/signup') 
    .post(AuthController.signup);

router.route('/auth/login')
    .post(AuthController.login);

router.route('/auth/forgotPassword')
    .post(AuthController.forgotPassword);

router.route('/auth/changePassword')
    .post(AuthController.changePassword);

router.route('/auth/updatePassword')
    .post(AuthController.updatePassword);

router.route('/updateOAuthToken')
    .post(AuthController.updateOAuthToken);

// router.route('/updateOAuthToken')
//     .post(AuthController.updateOAuthToken);




module.exports = router;