const express = require('express');
const router = express.Router();

const productMasterController = require('../../controllers/v0.1/productMaster.controller')

router.route("/addProduct")
    .post(productMasterController.addProduct);

router.route("/deleteProduct")
    .delete(productMasterController.deleteProduct);

router.route("/editProduct")
    .put(productMasterController.editProduct);

router.route("/getAllProduct")
    .get(productMasterController.getAllProduct);

router.route("/getProductById")
    .get(productMasterController.getProductById);


router.route("/addProductGroup")
    .post(productMasterController.addProductGroup);

router.route("/editProductGroup")
    .post(productMasterController.editProductGroup);

router.route("/getAllProductGroup")
    .get(productMasterController.getAllProductGroup);


module.exports = router;