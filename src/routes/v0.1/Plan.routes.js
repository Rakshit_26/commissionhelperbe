const express = require('express');
const router = express.Router();

const PlanController = require('../../controllers/v0.1/Plan.controller');

router.route('/addPlan')
.post(PlanController.addPlan);

router.route('/ModifyPlan')
.put(PlanController.ModifyPlan);

router.route('/obtainPlanById')
.get(PlanController.obtainPlanById);

router.route('/getAllPlans')
.get(PlanController.getAllPlans);

router.route('/removePlan')
.delete(PlanController.removePlan);

module.exports = router;