const express = require('express');
const router = express.Router();

const ImageController = require('../../controllers/v0.1/Image.controller');

router.route('/ProfilePicture')
.post(ImageController.ProfilePicture);
