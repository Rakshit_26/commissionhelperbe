const express = require('express');
const router = express.Router();

const PlanToTeamController = require('../../controllers/v0.1/PlanToTeam.controller')

router.route("/AddPlanToTeam")
.post(PlanToTeamController.AddPlanToTeam);

router.route("/RemovePlanFromTeam")
.delete(PlanToTeamController.RemovePlanFromTeam);

router.route("/ModifyPlanFromTeam")
.put(PlanToTeamController.ModifyPlanFromTeam);

router.route("SearchPlanFromTeam")
.get(PlanToTeamController.SearchPlanFromTeam);

module.exports = router;