const express = require('express');
const router = express.Router();

const TeamController = require('../../controllers/v0.1/Team.controller')

router.route("/addTeam")
.post(TeamController.addTeam);

router.route("/deleteTeam")
.delete(TeamController.deleteTeam);

router.route("/updateTeam")
.put(TeamController.updateTeam);

router.route("/getAllTeam")
.get(TeamController.getAllTeam);

router.route("/getTeamById")
.get(TeamController.getTeamById);

router.route("/getSubTeamById")
.get(TeamController.getSubTeamById);

router.route("/getAllSubTeam")
.get(TeamController.getAllSubTeam);
module.exports = router;