module.exports.UserStatus = {
  enabled: 0,
  disabled: 1,
  deleted: 2,
};

module.exports.UserType = {
  admin: 1,
  user: 2,
  agent: 3,
  salesRep: 4,


  
  // SFadmin: 1,
  // salesRep: 2,
  // SFManager: 3,
  // PAdmin:4,
  // PManager:5,
  // PAgent:6

};

module.exports.MsgType = {
  VMsg: 1,
  Greetings: 2
};

module.exports.callStatus = {
  available: 0,
  anotherCall: 1,
  onHold: 2
}
