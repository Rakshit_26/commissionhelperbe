const mongoose = require('mongoose');

const { Schema } = mongoose;

const PlanSchema = new Schema(
    {
        planName:{
            type: String
        },
        commissionAmount:{
            type: Number
        },
        users:[{
            type:Schema.Types.ObjectId,
            ref:"Users"
        }]
        // AddPlan: {
        //     type: String,
        //     required: true
        // },
        // // RenamePlan: {
        // //     type: Number,
        // // },
        // MonthStart: {
        //     type: Date
        // },
        // MonthEnd: {
        //     type: Date
        // },
        // Year: {
        //     type: Number,
        //     required: true
        // },
        // Region: {
        //     type: String,
        //     required: true
        // },
        // CommissionPercentage: {
        //     type: Number
        // },
        // // Facality: {
        // //     type: String
        // // },
        // StoreValues2:{
        //     type: Array
        // }
    },
    {
        timestamps: true
    }
);
module.exports = mongoose.model('Plan', PlanSchema)