const mongoose = require('mongoose');

const { Schema } = mongoose;

const UserToPlanSchema = new Schema(
    {
        Spiffer: {
            type: String,
            required: true,
            trim: true
        },
        Email: {
            type: String,
            required: true
        },
        role: {
            type: String,
            require: true  
        },
        EffectiveAsOf: {
            type: Date,
            default: Date.now()
        },
        EffectiveUntil: {
            type: Date,
            default: Date.now()
        },
    },
    {
        timestamps: true
    }
);
module.exports = mongoose.model('UserToPlan', UserToPlanSchema)