const mongoose = require("mongoose");
const { sep } = require("path");

const { Schema}=mongoose;

const QuotaSchema = new Schema({
    
    // quotaname:{
    //     type: String,
    //     require : true
    // },
    quotaname:{
        type: String
    },
    cadence:{
        type: String
    },
    names: [{
        type: Schema.Types.String,
        ref: "Users"
    }],
    price: [{
        type: Array
    }]
    // ,
    // name: [{
    //     type: Schema.Types.ObjectId,
    //     ref:'Users'
    // }]
},{
    timestamps: true
}

)
module.exports=mongoose.model('quota' , QuotaSchema);
  
