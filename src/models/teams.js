const mongoose = require("mongoose");

const { Schema } = mongoose;

const TeamSchema = new Schema({
    teamName: {
        type: String,
        unique: true
    },
    numberOfMember: {
        type: Number
    },
    createdBy: {
        type: Schema.Types.ObjectId,
        ref:'Users'
    },
    teamMemberName: {
        type: Array,
    },
    teamLeadName: {
        type: Array,
    },
    teamLeadID: [{
        type: Schema.Types.ObjectId,
        ref: 'Users',
    }],
    teamMemberID: [{
        type: Schema.Types.ObjectId,
        ref: 'Users',
    }],
    subTeamId:[{
        type: Schema.Types.ObjectId,
        ref: 'Team',
    }],
    subTeamName:[{
        type: Schema.Types.String,
        ref: 'Team',
    }]
}, 
{
    timestamps: true
}
);

module.exports = mongoose.model('Team', TeamSchema);