const mongoose = require('mongoose');

const { Schema } = mongoose;

const productMasterSchema = new Schema({

    productName: {
        type: String
    },
    price: {
        type: Number,
        // required: true
    },
    description: {
        type: String
    },
    unitQuantity: {
        type: Number,
        // required: true
    },
    productCode: {
        type: Number
    },
    priceBook: {
        type: String
    },
    category: {
        type: String,
    
    }
},
    {
        timestamps: true
    }
);
module.exports = mongoose.model('productMaster', productMasterSchema)