const mongoose = require('mongoose');

const { Schema } = mongoose;

const productGroupSchema = new Schema({

    GroupName: {
        type: String
    },
    productIds: [{
        type: mongoose.Schema.Types.ObjectId
    }],
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Users",
        required: true
    },
    updatedBy: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "Users"
    },
    createdAt: {
        type: Date,
        required: true,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        required: true,
        default: Date.now()
    },
    isActive: {
        type: Boolean
    },
    isDeleted: {
        type: Boolean
    },

},
    {
        timestamps: true
    }
);
module.exports = mongoose.model('productGroup', productGroupSchema)