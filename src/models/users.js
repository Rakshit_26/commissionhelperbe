const mongoose = require('mongoose');
const { UserStatus } = require('../datatypes/User');

const { Schema } = mongoose;

const UserSchema = new Schema(
    {
        Name: {
            type: String,
            trim: true
        },
        contactNumber: {
            type: Number
        },
        Company: {
            type: String
        },
        Role: {
            type: String,
        },
        // EffectiveAsOf: {
        //     type: Date,
        //     default: Date.now()
        // },
        // EffectiveUntil: {
        //     type: Date,
        //     default: Date.now()
        //},
        Address: {
            type: String
        },
        username: {
            type: String,
            trim: true
        },
        password: {
            type: String,
        },
        emailHash: {
            type: String
        },
        salt: {
            type: String
        },
        userType: {
            type: Number
        },
        SalesForceId: {
            type: String,
            default: null
        },
        createdAt: {
            type: Date,
            default: Date.now()
        },
        updatedAt: {
            type: Date,
            default: Date.now()
        },
        oauthToken: {
            type: String,
            default: null
        },
        refreshToken: {
            type: String,
            default: null
        },
        instanceUrl: {
            type: String,
            default: null
        },
        AccessToken: {
            type: String,
            default: null
        },
        isDeleted: {
            type: Boolean,
            default: false
        },
        isActive: {
            type: Boolean,
            default: false
        }
    },
    {
        timestamps: true
    },

);
module.exports = mongoose.model('Users', UserSchema);
