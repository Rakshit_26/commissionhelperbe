const mongoose = require('mongoose');

const { Schema } = mongoose;

const PlanToTeamSchema = new Schema(
    {
        PlanName: {
            type: String,
            required: true
        },
        Spiffer: {
            type: String,
            required: true,
            trim: true
        },
        Email: {
            type: String,
            required: true
        },
        role: {
            type: String,
            require: true  
        },
        EffectiveAsOf: {
            type: Number
        },
        EffectiveUntil: {
            type: Number
        },
        StoreValues2:{
            type: Array
        }
    },
    {
        timestamps: true
    }
);
module.exports = mongoose.model('PlanToTeam', PlanToTeamSchema)